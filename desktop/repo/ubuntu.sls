Transmission:
  pkgrepo.managed:
    - hunamname: Transmission
    - name:      deb http://ppa.launchpad.net/transmissionbt/ppa/ubuntu {{ grains['oscodename'] }} main
    - dist:      {{ grains['oscodename'] }}
    - file:      /etc/apt/sources.list.d/transmission.list
    - keyid:     365C5CA1
    - keyserver: keyserver.ubuntu.com
    - require_in:
      - pkg: desktop

Weechat:
  pkgrepo.managed:
    - hunamname: Weechat
    - name:      deb http://ppa.launchpad.net/nesthib/weechat-stable/ubuntu {{ grains['oscodename'] }} main
    - dist:      {{ grains['oscodename'] }}
    - file:      /etc/apt/sources.list.d/weechat.list
    - keyid:     4573289D
    - keyserver: keyserver.ubuntu.com
    - require_in:
      - pkg: desktop

mpv:
  pkgrepo.managed:
    - hunamname: mpv
    - name:      deb http://ppa.launchpad.net/mc3man/trusty-media/ubuntu {{ grains['oscodename'] }} main
    - dist:      {{ grains['oscodename'] }}
    - file:      /etc/apt/sources.list.d/mpv.list
    - keyid:     ED8E640A
    - keyserver: keyserver.ubuntu.com
    - require_in:
      - pkg: desktop

Photo:
  pkgrepo.managed:
    - hunamname: Photo
    - name:      deb http://ppa.launchpad.net/samrog131/ppa/ubuntu {{ grains['oscodename'] }} main
    - dist:      {{ grains['oscodename'] }}
    - file:      /etc/apt/sources.list.d/photo.list
    - keyid:     2B7E03A7
    - keyserver: keyserver.ubuntu.com
    - require_in:
      - pkg: desktop
