RPM_Fusion:
  pkg.installed:
    - sources:
      - rpmfusion-free-release:    http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-{{ grains['osrelease'] }}.noarch.rpm
      - rpmfusion-nonfree-release: http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-{{ grains['osrelease'] }}.noarch.rpm

Dropbox:
  pkg.installed:
    - sources:
      - nautilus-dropbox: https://linux.dropbox.com/packages/fedora/nautilus-dropbox-1.6.0-1.fedora.{{ grains['cpuarch'] }}.rpm
    - require_in:
      - pkg: desktop