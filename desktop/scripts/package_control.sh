#!/usr/bin/env bash

status() {
    if [[ $1 -ne 0 ]]; then
        echo "changed=no comment='$2'"
        exit $1
    fi
}

if [[ -f /home/{{ pillar['auser'] }}/.config/sublime-text-3/Installed\ Packages/Package\ Control.sublime-package ]]; then
        echo "changed=no comment='Sublime Package Control already installed'"
        exit 0
fi

mkdir -p /home/{{ pillar['auser'] }}/.config/sublime-text-3/Installed\ Packages
cd /home/{{ pillar['auser'] }}/.config/sublime-text-3/Installed\ Packages
status $? 'Could not create directory'

wget -q https://sublime.wbond.net/Package%20Control.sublime-package
status $? 'Could not download Sublime Package Control'

echo "changed=yes comment='Sublime Package Control installed'"
exit 0
