{% set ver = '3065' %}

# TODO - Assuming `grains['osarch']` on x32 is 'i386'
{% if grains['os_family'] == 'Debian' %}
  {% set arch = grains['osarch'] %}
{% else %}
  {% if grains['osarch'] == 'i386' %}
    {% set arch = 'x32' %}
  {% else %}
    {% set arch = 'x64' %}
  {% endif %}
{% endif %}

{% if grains['os_family'] == 'Debian' %}
sublime_text:
  pkg.installed:
    - sources:
      - sublime-text: http://c758482.r82.cf2.rackcdn.com/sublime-text_build-{{ ver }}_{{ arch }}.deb
{% else %}
sublime_text:
  pkg.latest:
    - pkgs:
      - curl
  cmd.run:
    - name:   curl -s http://c758482.r82.cf2.rackcdn.com/sublime_text_3_build_{{ ver }}_x{{ arch }}.tar.bz2 | tar xj
    - cwd:    /usr/local/lib64/
    - unless: test "$(subl --version 2>/dev/null)" = 'Sublime Text Build {{ ver }}'
    - require:
      - pkg: sublime_text

sublime_desktop:
  file.managed:
    - name:   /usr/local/share/applications/sublime-text.desktop
    - source: salt://desktop/sublime.desktop
    - user:   root
    - group:  root
    - mode:   644
    - require:
      - pkg: sublime_text

sublime_link:
  file.symlink:
    - name:   /usr/local/bin/subl
    - target: /usr/local/lib64/sublime_text_3/sublime_text
    - watch:
      - pkg: sublime_text
{% endif %}

sublime_package_control:
  cmd.script:
    - name:     salt://desktop/scripts/package_control.sh
    - soure:    salt://desktop/scripts/package_control.sh
    - template: jinja
    - user:     {{ pillar['auser'] }}
    - group:    {{ pillar['auser'] }}
    - stateful: True
    - watch:
      - pkg: sublime_text
