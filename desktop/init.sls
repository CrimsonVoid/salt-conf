{% if grains['os_family'] == 'RedHat' %}
include:
  - .repo.redhat
{% elif grains['os'] == 'Ubuntu' %}
include:
  - .repo.ubuntu
{% endif %}

desktop:
  pkg.latest:
    - refresh: True
    - pkgs:
      - gimp
      - keepassx
      - mpd
      - mpdscribble
      - mpv
      - nautilus-dropbox
      - ncmpcpp
      - rxvt-unicode-256color
      - transmission
      - weechat
      {% if grains['os_family'] == 'Debian' %}
      - photoqt
      {% endif %}

git_name:
  pkg.latest:
    - name: git
  git.config:
    - name:      user.name
    - value:     {{ pillar['git_name'] }}
    - user:      {{ pillar['auser'] }}
    - is_global: True
    - require:
      - pkg: git_name

git_email:
  pkg.latest:
    - name: git
  git.config:
    - name:      user.email
    - value:     {{ pillar['git_email'] }}
    - user:      {{ pillar['auser'] }}
    - is_global: True
    - require:
      - pkg: git_email
