{% set docker_repo = '/opt/docker' %}
{% set volumes = pillar['docker_volumes'] %}

include:
  - core.docker

docker_dir:
  file.directory:
    - name:      {{ docker_repo }}
    - user:      {{ pillar['auser'] }}
    - group:     adm
    - dir_mode:  0775
    - make_dirs: True

## Build images and deploy data containers
{% for img in 'nzbget', 'nzbdrone' %}
data_{{ img }}_build:
  git.latest:
    - name:   https://github.com/CrimsonVoid/Dockerfile
    - rev:    data-{{ img }}
    - target: {{ docker_repo }}/data/{{ img }}
    - user:   {{ pillar['auser'] }}
    - require:
      - file: docker_dir
  docker.built:
    - name: data/{{ img }}
    - path: {{ docker_repo }}/data/{{ img }}
    - watch:
      - git: data_{{ img }}_build
    - require:
      - pip: docker_py

{{ img }}_build:
  git.latest:
    - name:   https://github.com/CrimsonVoid/Dockerfile
    - rev:    {{ img }}
    - target: {{ docker_repo }}/{{ img }}
    - user:   {{ pillar['auser'] }}
    - require:
      - file: docker_dir
  docker.built:
    - name:    local/{{ img }}
    - path:    {{ docker_repo }}/{{ img }}
    - nocache: True
    - watch:
      - git: {{ img }}_build
    - require:
      - pip: docker_py

data_{{ img }}_deploy:
  docker.running:
    - name:             data.{{ img }}
    - image:            data/{{ img }}
    - check_is_running: False
    - volumes:
      {% for host, guest in volumes[img].iteritems() %}
      - {{ host }}: {{ guest }}
      {% endfor %}
    - watch:
      - docker: data_{{ img }}_build
    - require:
      - pip:    docker_py
{% endfor %}

## Deploy services
nzbget_deploy:
  docker.running:
    - name:       nzbget
    - image:      local/nzbget
    - stdin_open: True
    - tty:        True
    - ports:
      - 6789: 6789
    - volumes_from:
      - data.nzbget
    - watch:
      - docker: nzbget_build
      - docker: data_nzbget_deploy
    - require:
      - pip:    docker_py

nzbdrone_deploy:
  docker.running:
    - name:       nzbdrone
    - image:      local/nzbdrone
    - ports:
      - 8989: 8989
    - volumes_from:
      - data.nzbdrone
      - data.nzbget
    - links:
      nzbget: nzbget
    - watch:
      - docker: nzbdrone_build
      - docker: nzbget_deploy
      - docker: data_nzbdrone_deploy
      - docker: data_nzbget_deploy
    - require:
      - pip:    docker_py
