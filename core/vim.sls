vundle:
  pkg.latest:
    - name: git
  git.latest:
    - name:   https://github.com/gmarik/Vundle.vim.git
    - target: /home/{{ pillar['auser'] }}/.vim/bundle/Vundle.vim
    - user:   {{ pillar['auser'] }}
    - require:
      - pkg: vundle
