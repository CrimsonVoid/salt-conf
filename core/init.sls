core:
  pkg.latest:
    - refresh: true
    - pkgs:
      - curl
      - htop
      - git
      - tmux
      - unzip
      - zsh
      {% if grains['os_family'] == 'Debian' %}
      - sysv-rc-conf
      - vim
      {% elif grains['os_family'] == 'RedHat' %}
      - vim-enhanced
      {% endif %}

{{ pillar['auser'] }}:
  user.present:
    - remove_groups: False
    - shell:         /bin/{{ pillar['ashell'] }}
