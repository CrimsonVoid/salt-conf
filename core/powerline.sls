{% if grains['os_family'] == 'Debian' %}
{% set dev = 'dev' %}
{% elif grains['os_family'] == 'RedHat' %}
{% set dev = 'devel' %}
{% endif %}

psutil_deps:
  pkg.latest:
    - pkgs:
      - gcc
    - require_in:
      - pip: psutil

pygit2_deps:
  pkg.latest:
    - pkgs:
      - gcc
      - libgit2-{{ dev }}
      - python-{{ dev }}
    - require_in:
      - pip: pygit2

# Suggested packaged for powerline
{% for p in 'psutil', 'pygit2' %}
{{ p }}:
  pkg.latest:
    - name: python-pip
  pip.installed:
    - upgrade: True
    - user:    {{ pillar['auser'] }}
    - install_options: 
      - --user
    - require:
      - pkg: {{ p }}
{% endfor %}

powerline:
  pkg.latest:
    - pkgs:
      - gcc
      - git
      - python-pip
  pip.installed:
    - name:    git+git://github.com/Lokaltog/powerline
    - upgrade: True
    - user:    {{ pillar['auser'] }}
    - install_options:
      - --user
    - require:
      - pkg: powerline
