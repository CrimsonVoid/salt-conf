docker:
  {% if grains['os'] == 'Ubuntu' %}
  pkgrepo.managed:
    - humanname: Docker
    - name:      deb https://get.docker.io/ubuntu docker main
    - dist:      docker
    - file:      /etc/apt/sources.list.d/docker.list
    - keyid:     36A1D7869245C8950F966E92D8576A8BA88D21E9
    - keyserver: keyserver.ubuntu.com
    - require_in:
      - pkg: docker
  {% endif %}
  pkg.latest:
    - pkgs:
      {% if grains['os'] == 'Ubuntu' %}
      - lxc-docker
      {% elif grains['os'] == 'Debian' %}
      - docker.io
      {% elif grains['os_family'] == 'RedHat' %}
      - docker-io
      {% else %}
      - docker
      {% endif %}
  user.present:
    - name:          {{ pillar['auser'] }}
    - remove_groups: False
    - optional_groups:
      - docker
    - require:
      - pkg: docker

docker_py:
  pkg.latest:
    - name: python-pip
  pip.installed:
    - name: docker-py
    - user: {{ pillar['auser'] }}
    - install_options:
      - --user
    - require:
      - pkg: docker
      - pkg: docker_py

# FIX - Doesn't pull on first run
docker_ubuntu:
  docker.pulled:
    - name: ubuntu
    - tag:  14.04
    - user: {{ pillar['auser'] }}
    - require:
      - pip: docker_py
