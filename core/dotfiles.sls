dotfiles:
  pkg.latest:
    - name: git
  git.latest:
    - name:   {{ pillar['dotfiles_repo'] }}
    - rev:    master
    - target: /home/{{ pillar['auser'] }}/dotfiles
    - user:   {{ pillar['auser'] }}
    - require:
      - pkg: dotfiles
  cmd.wait:
    - name: {{ pillar['dotfiles_cmd'] }}
    - cwd:  /home/{{ pillar['auser'] }}/dotfiles
    - user: {{ pillar['auser'] }}
    - watch:
      - git: dotfiles

