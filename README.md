salt-conf
=========
Configure Linux boxes with Salt

## Salt
This repo is used to configure and install various software. To enable or disable
certain configurations comment out options in top.sls (with '#').


## Pillar
Pillar files are not included in this repo. The confs you will neeed to set are:
- auser  = User to configure. This will also be used for home directories
- ashell = The shell to be configured for `asuer`

- git_name  = Github name to be configured for `auser`
- git_email = Github email to be configured for `auser`

- dotfiles_repo = This will be cloned to /home/$auser/dotfiles
- dotfiles_cmd  = A command to run to install dotfiles, if necessary. This will
    be run in /home/$auser/dotfiles as $auser user

- docker_volumes = A dictionary of Docker images to a mapping of host:guest pairs
    to map in a container
