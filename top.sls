base:
  '*':
    - core
    - core.docker
    - core.dotfiles
    - core.powerline
    - core.vim
    # desktop
    # desktop.sublime
    # desktop.usenet
    # dev.cpp
    # dev.go
    # dev.python
    # dev.node
    # dev.haskell
    # dev.docker.cpp
    # dev.docker.go
    # dev.docker.rust
