{% if grains['os_family'] == 'Debian' %}
{% set dev = 'dev' %}
{% elif grains['os_family'] == 'RedHat' %}
{% set dev = 'devel' %}
{% endif %}

python:
  pkg.latest:
    - pkgs:
      - python-pip
      - python3-pip
      - python-{{ dev }}
      - python3-{{ dev }}

# FIX - Fails to install 'virtualenv'
{% for p in 'virtualenv', 'virtualenvwrapper', %}
{{ p }}:
  pip.installed:
    - user: {{ pillar['auser'] }}
    - install_options:
      - --user
    - require:
      - pkg: python
{% endfor %}
