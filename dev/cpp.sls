cpp:
  pkg.latest:
    - pkgs:
      - autoconf
      - automake
      - gcc
      - clang
      - make
      - cmake
      - libc++1
      - gdb
      - valgrind
      {% if grains['os_family'] == 'RedHat' %}
      - gcc-c++
      - glibc-devel
      {% elif grains['os_family'] == 'Debian' %}
      - g++
      - libc6-dev
      {% endif %}
