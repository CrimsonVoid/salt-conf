NodeJS:
  {% if grains['os'] == 'Ubuntu' %}
  pkgrepo.managed:
    - humanname: node
    - name:      deb http://ppa.launchpad.net/chris-lea/node.js/ubuntu {{ grains['oscodename'] }} main
    - dist:      {{ grains['oscodename'] }}
    - file:      /etc/apt/sources.list.d/nodejs.list
    - keyid:     C7917B12
    - keyserver: keyserver.ubuntu.com
    - require_in:
      - pkg: NodeJS
  {% endif %}
  pkg.latest:
    - pkgs:
      - nodejs
      {% if grains['os'] != 'Ubuntu' %}
      - npm
      {% endif %}
