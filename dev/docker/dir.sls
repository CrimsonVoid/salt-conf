{% set docker_repo = '/opt/docker/devel' %}

docker_dir:
  file.directory:
    - name:      {{ docker_repo }}
    - user:      {{ pillar['auser'] }}
    - group:     adm
    - dir_mode:  0775
    - make_dirs: True
