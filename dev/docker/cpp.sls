{% set img = 'cpp' %}
{% set docker_repo = '/opt/docker/devel/{}'.format(img) %}

include:
  - core.docker
  - dev.docker.dir

{{ img }}:
  git.latest:
    - name:   https://github.com/CrimsonVoid/Dockerfile
    - rev:    devel-{{ img }}
    - target: {{ docker_repo }}
    - user:   {{ pillar['auser'] }}
    - require:
      - file: docker_dir
  docker.built:
    - name: devel/{{ img }}
    - path: {{ docker_repo }}
    - user: {{ pillar['auser'] }}
    - watch:
      - git: {{ img }}
    - require:
      - pip: docker_py

{{ img }}_deploy:
  docker.installed:
    - name:       {{ img }}
    - image:      devel/{{ img }}
    - stdin_open: True
    - tty:        True
    - user:       {{ pillar['auser'] }}
    - volumes:
      - /home/{{ pillar['auser'] }}/code/{{ img }}: /home/lxc/code
    - require:
      - pip: docker_py
